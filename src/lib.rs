#[derive(Debug, Clone, PartialEq, Eq)]
enum Brace {
   Square,
   Round,
   Curly,
   Angle,
}

impl From<char> for Brace {
   fn from(ch: char) -> Self {
      match ch {
         '(' | ')' => Self::Round,
         '[' | ']' => Self::Square,
         '{' | '}' => Self::Curly,
         '<' | '>' => Self::Angle,
         _ => panic!["u wot m8"],
      }
   }
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum Symbol {
   BraceStart(Brace),
   BraceEnd(Brace),
   Period,
   Comma,
   // Colon,
   // SemiColon,
   // Slash,
   // Plus,
   // Minus,
   Equal,
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum TokenKind {
   Symbol(Symbol),
   Str(String),
   Int(bool, String),
   Float(bool, String),
   Ident(String),
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Token {
   pub line: usize,
   pub column: usize,
   pub kind: TokenKind,
}

impl Token {
   pub fn new(line: usize, column: usize, kind: TokenKind) -> Self {
      Self { line, column, kind }
   }
}

#[derive(Debug, Clone)]
pub enum ErrorKind {
   UnexpectedToken { expected: &'static str, got: String },
   UnicodeEscapeTooBig,
   InvalidUnicdeEscapeSequence,
   EmptyUnicodeEscapeSequence,
   UnmatchedEndBrace,
   UnmatchedStartBrace,
   InvalidEscapeCode,
}

#[derive(Debug, Clone)]
pub struct Error {
   eline: usize,
   ecolumn: usize,
   ekind: ErrorKind,
}

impl Error {
   pub fn new(eline: usize, ecolumn: usize, ekind: ErrorKind) -> Self {
      Self {
         eline,
         ecolumn,
         ekind,
      }
   }
}

#[derive(Debug)]
enum LexerStateKind {
   Any,
   ParsingNumber(bool, bool, String),
   // ParsingFloat(bool, String),
   ParsingIdent(String),
   ParsingStr(String),
   StrEscape,
   ByteEscape,
   ByteEscapeChar(char),
   UnicodeEscape,
   UnicodeEscapeStart,
   UnicodeEscapeStr(String),
   StartedBrace(Brace),
   Comment,
}

#[derive(Debug)]
struct LexerState {
   pub line: usize,
   pub column: usize,
   pub kind: LexerStateKind,
}

impl LexerState {
   pub fn new(line: usize, column: usize, kind: LexerStateKind) -> Self {
      Self { line, column, kind }
   }
}

enum Expected {
   Anything,
   UnicodeByteSequence,
   UnicodeByteSequenceBrace,
   HexNumber,
   AfterNum,
   AfterIdent,
   EndQuote,
}

impl From<Expected> for &'static str {
   fn from(e: Expected) -> &'static str {
      match e {
         Expected::Anything => "ident, string, comment, some kinda start brace, or a number",
         Expected::UnicodeByteSequence => "a unicode byte sequence",
         Expected::UnicodeByteSequenceBrace => "{",
         Expected::HexNumber => "a hex number",
         Expected::AfterNum => "a comma or brace, whitespace",
         Expected::AfterIdent => "a comma or brace, whitespace, more identifier chars",
         Expected::EndQuote => "an end quote",
      }
   }
}

struct Lexer<'a> {
   source: &'a str,
   state: Vec<LexerState>,
}
impl<'a> Lexer<'a> {
   pub fn new(source: &'a str) -> Self {
      Self {
         source,
         state: vec![LexerState {
            line: 0,
            column: 0,
            kind: LexerStateKind::Any,
         }],
      }
   }
   pub fn tokenize(&mut self) -> Result<Vec<Token>, Error> {
      let mut output: Vec<Token> = vec![];
      let mut current_line = 1;
      let mut current_column = 1;
      for c in self.source.chars() {
         if let Some(LexerState {
            line: _,
            column: _,
            kind: state,
         }) = self.state.last_mut()
         {
            match state {
               LexerStateKind::Any => match c {
                  '!' | '$' | '%' | '&' | '\'' | ')' | '*' | ',' | '.' | '/' | ':' | ';' | '>'
                  | '?' | '@' | '\\' | ']' | '^' | '_' | '`' | '|' | '}' | '~' => {
                     return Err(Error::new(
                        current_line,
                        current_column,
                        ErrorKind::UnexpectedToken {
                           expected: Expected::Anything.into(),
                           got: c.into(),
                        },
                     ))
                  }
                  '"' => self.state.push(LexerState::new(
                     current_line,
                     current_column,
                     LexerStateKind::ParsingStr(String::new()),
                  )),
                  '#' => self.state.push(LexerState::new(
                     current_line,
                     current_column,
                     LexerStateKind::Comment,
                  )),
                  '(' | '[' | '{' | '<' => {
                     self.state.push(LexerState::new(
                        current_line,
                        current_column,
                        LexerStateKind::StartedBrace(Brace::from(c)),
                     ));
                     output.push(Token::new(
                        current_line,
                        current_column,
                        TokenKind::Symbol(Symbol::BraceStart(Brace::from(c))),
                     ));
                  }
                  '+' => self.state.push(LexerState::new(
                     current_line,
                     current_column,
                     LexerStateKind::ParsingNumber(false, false, String::from(c)),
                  )),
                  '-' => {
                     if let Some(Token {
                        line: _,
                        column: _,
                        kind: TokenKind::Symbol(Symbol::Equal),
                     }) = output.last()
                     {
                        self.state.push(LexerState::new(
                           current_line,
                           current_column,
                           LexerStateKind::ParsingNumber(false, false, String::from(c)),
                        ));
                     } else {
                        self.state.push(LexerState::new(
                           current_line,
                           current_column,
                           LexerStateKind::ParsingIdent(String::from(c)),
                        ));
                     }
                  }
                  '0'..='9' => self.state.push(LexerState::new(
                     current_line,
                     current_column,
                     if let Some(Token {
                        line: _,
                        column: _,
                        kind,
                     }) = output.last()
                     {
                        match kind {
                           TokenKind::Symbol(symbol) => match symbol {
                              Symbol::BraceStart(brace) => match brace {
                                 Brace::Square => {
                                    LexerStateKind::ParsingNumber(false, false, c.into())
                                 }
                                 Brace::Round => {
                                    LexerStateKind::ParsingNumber(false, false, c.into())
                                 }
                                 Brace::Curly => LexerStateKind::ParsingIdent(c.into()),
                                 Brace::Angle => LexerStateKind::ParsingIdent(c.into()),
                              },
                              Symbol::BraceEnd(_) | Symbol::Period => {
                                 LexerStateKind::ParsingIdent(c.into())
                              }
                              // Symbol::Colon
                              // | Symbol::SemiColon
                              // | Symbol::Slash
                              // | Symbol::Plus
                              // | Symbol::Minus => unreachable![],
                              Symbol::Comma | Symbol::Equal => {
                                 LexerStateKind::ParsingNumber(false, false, c.into())
                              }
                           },
                           TokenKind::Str(_)
                           | TokenKind::Int(_, _)
                           | TokenKind::Float(_, _)
                           | TokenKind::Ident(_) => {
                              LexerStateKind::ParsingNumber(false, false, c.into())
                           }
                        }
                     } else {
                        LexerStateKind::ParsingIdent(c.into())
                     },
                  )),
                  '=' => output.push(Token::new(
                     current_line,
                     current_column,
                     TokenKind::Symbol(Symbol::Equal),
                  )),
                  _ if c.is_whitespace() => {}
                  _ => self.state.push(LexerState::new(
                     current_line,
                     current_column,
                     LexerStateKind::ParsingIdent(String::from(c)),
                  )),
               },

               LexerStateKind::ParsingNumber(hex, float, s) => match c {
                  '#' => {
                     output.push(
                        if let LexerState {
                           line: local_line,
                           column: local_column,
                           kind: LexerStateKind::ParsingNumber(hex, float, s),
                        } = self.state.pop().unwrap()
                        {
                           Token::new(
                              local_line,
                              local_column,
                              if float {
                                 TokenKind::Float(hex, s)
                              } else {
                                 TokenKind::Int(hex, s)
                              },
                           )
                        } else {
                           unreachable![]
                        },
                     );
                     self.state.push(LexerState::new(
                        current_line,
                        current_column,
                        LexerStateKind::Comment,
                     ));
                  }
                  '(' | '<' | '[' | '{' => {
                     output.push(
                        if let LexerState {
                           line: local_line,
                           column: local_column,
                           kind: LexerStateKind::ParsingNumber(hex, float, s),
                        } = self.state.pop().unwrap()
                        {
                           Token::new(
                              local_line,
                              local_column,
                              if float {
                                 TokenKind::Float(hex, s)
                              } else {
                                 TokenKind::Int(hex, s)
                              },
                           )
                        } else {
                           unreachable![]
                        },
                     );
                     self.state.push(LexerState::new(
                        current_line,
                        current_column,
                        LexerStateKind::StartedBrace(Brace::from(c)),
                     ));
                     output.push(Token::new(
                        current_line,
                        current_column,
                        TokenKind::Symbol(Symbol::BraceStart(Brace::from(c))),
                     ));
                  }
                  ')' | '>' | ']' | '}' => {
                     output.push(
                        if let LexerState {
                           line: local_line,
                           column: local_column,
                           kind: LexerStateKind::ParsingNumber(hex, float, s),
                        } = self.state.pop().unwrap()
                        {
                           Token::new(
                              local_line,
                              local_column,
                              if float {
                                 TokenKind::Float(hex, s)
                              } else {
                                 TokenKind::Int(hex, s)
                              },
                           )
                        } else {
                           unreachable![]
                        },
                     );
                     if let Some(LexerState {
                        line: local_line,
                        column: local_column,
                        kind: LexerStateKind::StartedBrace(b),
                     }) = self.state.last()
                     {
                        if Brace::from(c) == *b {
                           output.push(Token::new(
                              current_line,
                              current_column,
                              TokenKind::Symbol(Symbol::BraceEnd(b.clone())),
                           ))
                        } else {
                           return Err(Error::new(
                              *local_line,
                              *local_column,
                              ErrorKind::UnmatchedEndBrace,
                           ));
                        }
                        self.state.pop();
                     } else {
                        return Err(Error::new(
                           current_line,
                           current_column,
                           ErrorKind::UnmatchedEndBrace,
                        ));
                     }
                  }
                  ',' => {
                     output.push(
                        if let LexerState {
                           line: local_line,
                           column: local_column,
                           kind: LexerStateKind::ParsingNumber(hex, float, s),
                        } = self.state.pop().unwrap()
                        {
                           Token::new(
                              local_line,
                              local_column,
                              if float {
                                 TokenKind::Float(hex, s)
                              } else {
                                 TokenKind::Int(hex, s)
                              },
                           )
                        } else {
                           unreachable![]
                        },
                     );
                     output.push(Token::new(
                        current_line,
                        current_column,
                        TokenKind::Symbol(Symbol::Comma),
                     ));
                  }
                  '.' => {
                     if !*float {
                        *float = true;
                        s.push(c);
                     } else {
                        output.push(
                           if let LexerState {
                              line: local_line,
                              column: local_column,
                              kind: LexerStateKind::ParsingNumber(_, _, s),
                           } = self.state.pop().unwrap()
                           {
                              Token::new(local_line, local_column, TokenKind::Ident(s))
                           } else {
                              unreachable!["waaaaat? but we guarded!"]
                           },
                        );
                        output.push(Token::new(
                           current_line,
                           current_column,
                           TokenKind::Symbol(Symbol::Period),
                        ))
                     }
                  }
                  '0'..='9' => s.push(c),
                  'A'..='F' | 'a'..='f' | 'x' => {
                     *hex = true;
                     s.push(c)
                  }
                  '-' => {
                     s.push(c);
                     let ls = self.state.pop().unwrap();
                     self.state.push(
                        if let LexerState {
                           line: local_line,
                           column: local_column,
                           kind: LexerStateKind::ParsingNumber(_, _, s),
                        } = ls
                        {
                           LexerState {
                              line: local_line,
                              column: local_column,
                              kind: LexerStateKind::ParsingIdent(s),
                           }
                        } else {
                           unreachable![""]
                        },
                     );
                  }
                  c if c.is_whitespace() => {
                     output.push(
                        if let LexerState {
                           line: local_line,
                           column: local_column,
                           kind: LexerStateKind::ParsingNumber(hex, float, s),
                        } = self.state.pop().unwrap()
                        {
                           Token::new(
                              local_line,
                              local_column,
                              if float {
                                 TokenKind::Float(hex, s)
                              } else {
                                 TokenKind::Int(hex, s)
                              },
                           )
                        } else {
                           unreachable![]
                        },
                     );
                  }
                  _ => {
                     return Err(Error {
                        eline: current_line,
                        ecolumn: current_column,
                        ekind: ErrorKind::UnexpectedToken {
                           expected: Expected::AfterNum.into(),
                           got: c.into(),
                        },
                     })
                  }
               },
               LexerStateKind::ParsingStr(s) => match c {
                  '"' => {
                     output.push(
                        if let LexerState {
                           line: iline,
                           column: icolumn,
                           kind: LexerStateKind::ParsingStr(s),
                        } = self.state.pop().unwrap()
                        {
                           Token::new(iline, icolumn, TokenKind::Str(s))
                        } else {
                           unreachable![]
                        },
                     );
                  }
                  '\\' => self.state.push(LexerState::new(
                     current_line,
                     current_column,
                     LexerStateKind::StrEscape,
                  )),
                  _ => s.push(c),
               },
               LexerStateKind::StrEscape => {
                  self.state.pop();
                  if let Some(LexerState {
                     line: _,
                     column: _,
                     kind: LexerStateKind::ParsingStr(s),
                  }) = self.state.last_mut()
                  {
                     match c {
                        '"' => s.push(c),
                        '0' => s.push('\0'),
                        '\\' => s.push(c),
                        'a' => s.push('\x07'),
                        'b' => s.push('\x08'),
                        'f' => s.push('\x0C'),
                        'n' => s.push('\n'),
                        'r' => s.push('\r'),
                        't' => s.push('\t'),
                        'u' => self.state.push(LexerState::new(
                           current_line,
                           current_column,
                           LexerStateKind::UnicodeEscape,
                        )),
                        'v' => s.push('\x0B'),
                        'x' => self.state.push(LexerState::new(
                           current_line,
                           current_column,
                           LexerStateKind::ByteEscape,
                        )),
                        '!' | '\'' | '#' | '$' | '%' | '&' | '(' | ')' | '*' | '+' | ',' | '-'
                        | '.' | '/' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | ':'
                        | ';' | '<' | '=' | '>' | '?' | '@' | 'A' | 'B' | 'C' | 'D' | 'E' | 'F'
                        | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R'
                        | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z' | '[' | ']' | '^' | '_'
                        | '`' | 'c' | 'd' | 'e' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'o'
                        | 'p' | 'q' | 's' | 'w' | 'y' | 'z' | '{' | '|' | '}' | '~' => {
                           return Err(Error::new(
                              current_line,
                              current_column,
                              ErrorKind::InvalidEscapeCode,
                           ))
                        }
                        c if c.is_whitespace() => {
                           return Err(Error::new(
                              current_line,
                              current_column,
                              ErrorKind::InvalidEscapeCode,
                           ))
                        }
                        _ => {
                           return Err(Error::new(
                              current_line,
                              current_column,
                              ErrorKind::InvalidEscapeCode,
                           ))
                        }
                     }
                  } else {
                     unreachable!["pushed escape state while not in string mode. c'mon, meeeee"]
                  }
               }
               LexerStateKind::ByteEscape => match c {
                  '0'..='9' | 'A'..='F' | 'a'..='f' => {
                     self.state.pop();
                     self.state.push(LexerState::new(
                        current_line,
                        current_column,
                        LexerStateKind::ByteEscapeChar(c),
                     ));
                  }
                  _ => {
                     return Err(Error::new(
                        current_line,
                        current_column,
                        ErrorKind::UnexpectedToken {
                           expected: Expected::HexNumber.into(),
                           got: c.into(),
                        },
                     ))
                  }
               },
               LexerStateKind::ByteEscapeChar(_) => match c {
                  '0'..='9' | 'A'..='F' | 'a'..='f' => {
                     if let Some(LexerState {
                        line: _,
                        column: _,
                        kind: LexerStateKind::ByteEscapeChar(ch),
                     }) = self.state.pop()
                     {
                        if let Some(LexerState {
                           line: _,
                           column: _,
                           kind: LexerStateKind::ParsingStr(s),
                        }) = self.state.last_mut()
                        {
                           s.push(char::from(
                              u8::from_str_radix(&format!["{}{}", ch, c], 16).unwrap(),
                           ))
                        } else {
                           unreachable![]
                        }
                     }
                  }
                  _ => {
                     return Err(Error::new(
                        current_line,
                        current_column,
                        ErrorKind::UnexpectedToken {
                           expected: Expected::HexNumber.into(),
                           got: c.into(),
                        },
                     ))
                  }
               },
               LexerStateKind::UnicodeEscape => match c {
                  '{' => {
                     self.state.pop();
                     self.state.push(LexerState::new(
                        current_line,
                        current_column,
                        LexerStateKind::UnicodeEscapeStart,
                     ));
                  }
                  _ => {
                     return Err(Error::new(
                        current_line,
                        current_column,
                        ErrorKind::UnexpectedToken {
                           expected: Expected::UnicodeByteSequenceBrace.into(),
                           got: c.into(),
                        },
                     ))
                  }
               },
               LexerStateKind::UnicodeEscapeStart => match c {
                  '0'..='9' | 'A'..='F' | 'a'..='f' => {
                     self.state.pop();
                     self.state.push(LexerState::new(
                        current_line,
                        current_column,
                        LexerStateKind::UnicodeEscapeStr(c.into()),
                     ));
                  }
                  '}' => {
                     return Err(Error::new(
                        current_line,
                        current_column,
                        ErrorKind::EmptyUnicodeEscapeSequence,
                     ))
                  }
                  _ => {
                     return Err(Error::new(
                        current_line,
                        current_column,
                        ErrorKind::UnexpectedToken {
                           expected: Expected::UnicodeByteSequence.into(),
                           got: c.into(),
                        },
                     ))
                  }
               },
               LexerStateKind::UnicodeEscapeStr(s) => match c {
                  '0'..='9' | 'A'..='F' | 'a'..='f' => s.push(c),
                  '}' => {
                     if let Some(LexerState {
                        line: iline,
                        column: icolumn,
                        kind: LexerStateKind::UnicodeEscapeStr(e),
                     }) = self.state.pop()
                     {
                        if let Some(LexerState {
                           line: _,
                           column: _,
                           kind: LexerStateKind::ParsingStr(s),
                        }) = self.state.last_mut()
                        {
                           if let Ok(num) = u32::from_str_radix(&e, 16) {
                              if let Some(ch) = std::char::from_u32(num) {
                                 s.push(ch);
                              } else {
                                 return Err(Error::new(
                                    iline,
                                    icolumn,
                                    ErrorKind::InvalidUnicdeEscapeSequence,
                                 ));
                              }
                           } else {
                              return Err(Error::new(
                                 iline,
                                 icolumn,
                                 ErrorKind::UnicodeEscapeTooBig,
                              ));
                           }
                        } else {
                           panic!["oh no! You got a boo-boo! I'm sorry D:"]
                        }
                     } else {
                        panic!["oh my word are you ok? :/"]
                     }
                  }
                  _ => {
                     return Err(Error::new(
                        current_line,
                        current_column,
                        ErrorKind::UnexpectedToken {
                           expected: Expected::HexNumber.into(),
                           got: c.into(),
                        },
                     ))
                  }
               },
               LexerStateKind::ParsingIdent(s) => match c {
                  '#' => {
                     output.push(
                        if let LexerState {
                           line: iline,
                           column: icolumn,
                           kind: LexerStateKind::ParsingIdent(s),
                        } = self.state.pop().unwrap()
                        {
                           Token::new(iline, icolumn, TokenKind::Ident(s))
                        } else {
                           unreachable![]
                        },
                     );
                     self.state.push(LexerState::new(
                        current_line,
                        current_column,
                        LexerStateKind::Comment,
                     ));
                  }
                  '(' | '<' | '[' | '{' => {
                     output.push(
                        if let LexerState {
                           line: iline,
                           column: icolumn,
                           kind: LexerStateKind::ParsingIdent(s),
                        } = self.state.pop().unwrap()
                        {
                           Token::new(iline, icolumn, TokenKind::Ident(s))
                        } else {
                           unreachable![]
                        },
                     );
                     self.state.push(LexerState::new(
                        current_line,
                        current_column,
                        LexerStateKind::StartedBrace(Brace::from(c)),
                     ));
                     output.push(Token::new(
                        current_line,
                        current_column,
                        TokenKind::Symbol(Symbol::BraceStart(Brace::from(c))),
                     ));
                  }
                  ')' | '>' | ']' | '}' => {
                     output.push(
                        if let LexerState {
                           line: iline,
                           column: icolumn,
                           kind: LexerStateKind::ParsingIdent(s),
                        } = self.state.pop().unwrap()
                        {
                           Token::new(iline, icolumn, TokenKind::Ident(s))
                        } else {
                           unreachable![]
                        },
                     );
                     if let Some(LexerState {
                        line: _,
                        column: _,
                        kind: LexerStateKind::StartedBrace(b),
                     }) = self.state.last()
                     {
                        if Brace::from(c) == *b {
                           output.push(Token::new(
                              current_line,
                              current_column,
                              TokenKind::Symbol(Symbol::BraceEnd(b.clone())),
                           ))
                        } else {
                           return Err(Error::new(
                              current_line,
                              current_column,
                              ErrorKind::UnmatchedEndBrace,
                           ));
                        }
                        self.state.pop();
                     } else {
                        return Err(Error::new(
                           current_line,
                           current_column,
                           ErrorKind::UnmatchedEndBrace,
                        ));
                     }
                  }
                  '-' => s.push(c),
                  '.' => {
                     output.push(
                        if let LexerState {
                           line: iline,
                           column: icolumn,
                           kind: LexerStateKind::ParsingIdent(s),
                        } = self.state.pop().unwrap()
                        {
                           Token::new(iline, icolumn, TokenKind::Ident(s))
                        } else {
                           unreachable![]
                        },
                     );
                     output.push(Token::new(
                        current_line,
                        current_column,
                        TokenKind::Symbol(Symbol::Period),
                     ));
                  }
                  'a'..='z' | '0'..='9' | 'A'..='Z' => s.push(c),
                  c if c.is_whitespace() => output.push(
                     if let LexerState {
                        line: iline,
                        column: icolumn,
                        kind: LexerStateKind::ParsingIdent(s),
                     } = self.state.pop().unwrap()
                     {
                        Token::new(iline, icolumn, TokenKind::Ident(s))
                     } else {
                        unreachable![]
                     },
                  ),
                  '_' => s.push(c),
                  '=' => {
                     output.push(
                        if let LexerState {
                           line: iline,
                           column: icolumn,
                           kind: LexerStateKind::ParsingIdent(s),
                        } = self.state.pop().unwrap()
                        {
                           Token::new(iline, icolumn, TokenKind::Ident(s))
                        } else {
                           unreachable![]
                        },
                     );
                     output.push(Token::new(
                        current_line,
                        current_column,
                        TokenKind::Symbol(Symbol::Equal),
                     ));
                  }
                  '!' | '"' | '$' | '%' | '&' | '\'' | '*' | '+' | ',' | '/' | ':' | ';' | '?'
                  | '@' | '\\' | '^' | '`' | '|' | '~' => {
                     return Err(Error::new(
                        current_line,
                        current_column,
                        ErrorKind::UnexpectedToken {
                           expected: Expected::AfterIdent.into(),
                           got: c.into(),
                        },
                     ))
                  }
                  _ => s.push(c),
               },
               LexerStateKind::StartedBrace(_) => match c {
                  '"' => self.state.push(LexerState::new(
                     current_line,
                     current_column,
                     LexerStateKind::ParsingStr(String::new()),
                  )),
                  '#' => self.state.push(LexerState::new(
                     current_line,
                     current_column,
                     LexerStateKind::Comment,
                  )),
                  '(' | '<' | '[' | '{' => {
                     self.state.push(LexerState::new(
                        current_line,
                        current_column,
                        LexerStateKind::StartedBrace(Brace::from(c)),
                     ));
                     output.push(Token::new(
                        current_line,
                        current_column,
                        TokenKind::Symbol(Symbol::BraceStart(Brace::from(c))),
                     ));
                  }
                  ')' | '>' | ']' | '}' => {
                     if let Some(LexerState {
                        line: _,
                        column: _,
                        kind: LexerStateKind::StartedBrace(b),
                     }) = self.state.pop()
                     {
                        if Brace::from(c) == b {
                           output.push(Token::new(
                              current_line,
                              current_column,
                              TokenKind::Symbol(Symbol::BraceEnd(b.clone())),
                           ))
                        } else {
                           return Err(Error::new(
                              current_line,
                              current_column,
                              ErrorKind::UnmatchedEndBrace,
                           ));
                        }
                     }
                  }
                  '+' => self.state.push(LexerState::new(
                     current_line,
                     current_column,
                     LexerStateKind::ParsingNumber(false, false, c.into()),
                  )),
                  ',' => output.push(Token::new(
                     current_line,
                     current_column,
                     TokenKind::Symbol(Symbol::Comma),
                  )),

                  '0'..='9' => self.state.push(LexerState::new(
                     current_line,
                     current_column,
                     if let Some(Token {
                        line: _,
                        column: _,
                        kind,
                     }) = output.last()
                     {
                        match kind {
                           TokenKind::Symbol(symbol) => match symbol {
                              Symbol::BraceStart(brace) => match brace {
                                 Brace::Square => {
                                    LexerStateKind::ParsingNumber(false, false, c.into())
                                 }
                                 Brace::Round => {
                                    LexerStateKind::ParsingNumber(false, false, c.into())
                                 }
                                 Brace::Curly => LexerStateKind::ParsingIdent(c.into()),
                                 Brace::Angle => LexerStateKind::ParsingIdent(c.into()),
                              },
                              Symbol::BraceEnd(_) | Symbol::Period => {
                                 LexerStateKind::ParsingIdent(c.into())
                              }
                              // Symbol::Colon
                              // | Symbol::SemiColon
                              // | Symbol::Slash
                              // | Symbol::Plus
                              // | Symbol::Minus => unreachable![],
                              Symbol::Comma | Symbol::Equal => {
                                 LexerStateKind::ParsingNumber(false, false, c.into())
                              }
                           },
                           TokenKind::Str(_)
                           | TokenKind::Int(_, _)
                           | TokenKind::Float(_, _)
                           | TokenKind::Ident(_) => {
                              LexerStateKind::ParsingNumber(false, false, c.into())
                           }
                        }
                     } else {
                        LexerStateKind::ParsingIdent(c.into())
                     },
                  )),
                  '=' => output.push(Token::new(
                     current_line,
                     current_column,
                     TokenKind::Symbol(Symbol::Equal),
                  )),
                  _ if c.is_ascii_alphabetic() => self.state.push(LexerState::new(
                     current_line,
                     current_column,
                     LexerStateKind::ParsingIdent(c.into()),
                  )),
                  '!' | '$' | '%' | '&' | '\'' | '*' | '-' | '.' | '/' | ':' | ';' | '?' | '@'
                  | '\\' | '^' | '_' | '`' | '|' | '~' => {
                     return Err(Error::new(
                        current_line,
                        current_column,
                        ErrorKind::UnexpectedToken {
                           expected: Expected::Anything.into(),
                           got: c.into(),
                        },
                     ))
                  }
                  c if c.is_whitespace() => (),
                  _ => self.state.push(LexerState::new(
                     current_line,
                     current_column,
                     LexerStateKind::ParsingIdent(String::from(c)),
                  )),
               },
               LexerStateKind::Comment => {
                  if let '\n' | '\r' = c {
                     self.state.pop();
                  }
               }
            }
         }
         if c == '\n' {
            current_line += 1;
            current_column = 0;
         }
         current_column += 1;
      }
      if let Some(state) = self.state.pop() {
         match state.kind {
            LexerStateKind::Any => (),
            LexerStateKind::ParsingNumber(hex, float, s) => output.push(Token::new(
               state.line,
               state.column,
               if float {
                  TokenKind::Float(hex, s)
               } else {
                  TokenKind::Int(hex, s)
               },
            )),

            LexerStateKind::ParsingIdent(s) => {
               output.push(Token::new(state.line, state.column, TokenKind::Ident(s)))
            }
            LexerStateKind::ParsingStr(_)
            | LexerStateKind::StrEscape
            | LexerStateKind::ByteEscape
            | LexerStateKind::ByteEscapeChar(_)
            | LexerStateKind::UnicodeEscape
            | LexerStateKind::UnicodeEscapeStart
            | LexerStateKind::UnicodeEscapeStr(_) => {
               return Err(Error::new(
                  current_line,
                  current_column,
                  ErrorKind::UnexpectedToken {
                     expected: Expected::EndQuote.into(),
                     got: "end of file".into(),
                  },
               ))
            }
            LexerStateKind::StartedBrace(_) => {
               return Err(Error::new(
                  current_line,
                  current_column,
                  ErrorKind::UnmatchedStartBrace,
               ))
            }
            LexerStateKind::Comment => (),
         }
      }
      Ok(output)
   }
}

pub fn parse(source: &str) -> Result<(), Error> {
   let mut lexer = Lexer::new(source);
   let mut _tokens = lexer.tokenize()?;

   Ok(())
}

#[cfg(test)]
mod tests {
   use super::*;
   use pretty_assertions::assert_eq;
   #[test]
   fn idkwid() {
      let tokens = Lexer::new(
         "# Dis iz muh fuwwy beawr!!! :D
[fuwwy_beawr]
name = \"elfein\"
phrase = \"OwO multi
line text UwU\"
age = 22
age_in_hex = 0x16
favorite_number = 0x25
some_cool_hex_numbers = [ # idk I just think thay r fun
	0xA0,
	0xf.8 , # the space stays plz put back if format
	0x100.1 # hex floats r the best
]

[fuwwy_beawr.paws]
claws = \"sharp\"

[Īndigo]
description = \"one of the prettiest blues ever! :D\" # so pretty imo plz no hate
-properties = {
	wave-length = \"445 nm\"
	is-color}
some_other-stuff = {
	number = 4
}",
      )
      .tokenize()
      .unwrap();
      assert_eq![
         tokens,
         vec![
            Token::new(2, 1, TokenKind::Symbol(Symbol::BraceStart(Brace::Square))),
            Token::new(2, 2, TokenKind::Ident("fuwwy_beawr".into())),
            Token::new(2, 13, TokenKind::Symbol(Symbol::BraceEnd(Brace::Square))),
            Token::new(3, 1, TokenKind::Ident("name".into())),
            Token::new(3, 6, TokenKind::Symbol(Symbol::Equal)),
            Token::new(3, 8, TokenKind::Str("elfein".into())),
            Token::new(4, 1, TokenKind::Ident("phrase".into())),
            Token::new(4, 8, TokenKind::Symbol(Symbol::Equal)),
            Token::new(4, 10, TokenKind::Str("OwO multi\nline text UwU".into())),
            Token::new(6, 1, TokenKind::Ident("age".into())),
            Token::new(6, 5, TokenKind::Symbol(Symbol::Equal)),
            Token::new(6, 7, TokenKind::Int(false, "22".into())),
            Token::new(7, 1, TokenKind::Ident("age_in_hex".into())),
            Token::new(7, 12, TokenKind::Symbol(Symbol::Equal)),
            Token::new(7, 14, TokenKind::Int(true, "0x16".into())),
            Token::new(8, 1, TokenKind::Ident("favorite_number".into())),
            Token::new(8, 17, TokenKind::Symbol(Symbol::Equal)),
            Token::new(8, 19, TokenKind::Int(true, "0x25".into())),
            Token::new(9, 1, TokenKind::Ident("some_cool_hex_numbers".into())),
            Token::new(9, 23, TokenKind::Symbol(Symbol::Equal)),
            Token::new(9, 25, TokenKind::Symbol(Symbol::BraceStart(Brace::Square))),
            Token::new(10, 2, TokenKind::Int(true, "0xA0".into())),
            Token::new(10, 6, TokenKind::Symbol(Symbol::Comma)),
            Token::new(11, 2, TokenKind::Float(true, "0xf.8".into())),
            Token::new(11, 8, TokenKind::Symbol(Symbol::Comma)),
            Token::new(12, 2, TokenKind::Float(true, "0x100.1".into())),
            Token::new(13, 1, TokenKind::Symbol(Symbol::BraceEnd(Brace::Square))),
            Token::new(15, 1, TokenKind::Symbol(Symbol::BraceStart(Brace::Square))),
            Token::new(15, 2, TokenKind::Ident("fuwwy_beawr".into())),
            Token::new(15, 13, TokenKind::Symbol(Symbol::Period)),
            Token::new(15, 14, TokenKind::Ident("paws".into())),
            Token::new(15, 18, TokenKind::Symbol(Symbol::BraceEnd(Brace::Square))),
            Token::new(16, 1, TokenKind::Ident("claws".into())),
            Token::new(16, 7, TokenKind::Symbol(Symbol::Equal)),
            Token::new(16, 9, TokenKind::Str("sharp".into())),
            Token::new(18, 1, TokenKind::Symbol(Symbol::BraceStart(Brace::Square))),
            Token::new(18, 2, TokenKind::Ident("Īndigo".into())),
            Token::new(18, 8, TokenKind::Symbol(Symbol::BraceEnd(Brace::Square))),
            Token::new(19, 1, TokenKind::Ident("description".into())),
            Token::new(19, 13, TokenKind::Symbol(Symbol::Equal)),
            Token::new(
               19,
               15,
               TokenKind::Str("one of the prettiest blues ever! :D".into())
            ),
            Token::new(20, 1, TokenKind::Ident("-properties".into())),
            Token::new(20, 13, TokenKind::Symbol(Symbol::Equal)),
            Token::new(20, 15, TokenKind::Symbol(Symbol::BraceStart(Brace::Curly))),
            Token::new(21, 2, TokenKind::Ident("wave-length".into())),
            Token::new(21, 14, TokenKind::Symbol(Symbol::Equal)),
            Token::new(21, 16, TokenKind::Str("445 nm".into())),
            Token::new(22, 2, TokenKind::Ident("is-color".into())),
            Token::new(22, 10, TokenKind::Symbol(Symbol::BraceEnd(Brace::Curly))),
            Token::new(23, 1, TokenKind::Ident("some_other-stuff".into())),
            Token::new(23, 18, TokenKind::Symbol(Symbol::Equal)),
            Token::new(23, 20, TokenKind::Symbol(Symbol::BraceStart(Brace::Curly))),
            Token::new(24, 2, TokenKind::Ident("number".into())),
            Token::new(24, 9, TokenKind::Symbol(Symbol::Equal)),
            Token::new(24, 11, TokenKind::Int(false, "4".into())),
            Token::new(25, 1, TokenKind::Symbol(Symbol::BraceEnd(Brace::Curly))),
         ]
      ];
   }

   #[test]
   #[should_panic]
   fn isdkwid() {
      Lexer::new("\"\\u{}\"").tokenize().unwrap();
   }

   #[test]
   #[should_panic]
   fn irdkwid() {
      Lexer::new("}").tokenize().unwrap();
   }

   const ALLOWED_CHARS: [char; 95] = [
      '\n', ' ', '!', '"', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1',
      '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C',
      'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
      'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g',
      'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
      'z', '{', '|', '}', '~',
   ];

   #[test]
   fn fuzzy_wuzzy() {
      use rand::Rng;
      let mut rng = rand::thread_rng();
      let mut text = String::with_capacity(256);
      for _ in 0..10000 {
         for _ in 0..256 {
            text.push(ALLOWED_CHARS[(rng.gen::<f32>() * ALLOWED_CHARS.len() as f32) as usize]);
         }
         if let Ok(_tokens) = Lexer::new(&text).tokenize() {
            println!["Fuzz search succeeded with input: {}", text];
         }
         text.clear();
      }
   }

   #[test]
   #[should_panic]
   fn see_if_reachable() {
      Lexer::new("00}").tokenize().unwrap();
   }

   #[test]
   fn dash_converts_num_to_ident() {
      assert_eq![
         Lexer::new(
            "0-0 = 1
0.0- = 2
3.0.0 = 4"
         )
         .tokenize()
         .unwrap(),
         vec![
            Token::new(1, 1, TokenKind::Ident("0-0".into())),
            Token::new(1, 5, TokenKind::Symbol(Symbol::Equal)),
            Token::new(1, 7, TokenKind::Int(false, "1".into())),
            Token::new(2, 1, TokenKind::Ident("0.0-".into())),
            Token::new(2, 6, TokenKind::Symbol(Symbol::Equal)),
            Token::new(2, 8, TokenKind::Int(false, "2".into())),
            Token::new(3, 1, TokenKind::Ident("3.0".into())),
            Token::new(3, 4, TokenKind::Symbol(Symbol::Period)),
            Token::new(3, 5, TokenKind::Ident("0".into())),
            Token::new(3, 7, TokenKind::Symbol(Symbol::Equal)),
            Token::new(3, 9, TokenKind::Int(false, "4".into())),
         ]
      ];
   }
   #[test]
   fn fool_me_arbitrarily_many_times() {
      Lexer::new(r#""#).tokenize().unwrap();
   }
}
